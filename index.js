function getMyEle(id) {
    return document.getElementById(id);
}

var myArr = [];

// Tạo sự kiện click cho button sắp xếp mới xuất hiện
function swapViTri() {
    var viTri1 = getMyEle("viTri1").value * 1;
    var viTri2 = getMyEle("viTri2").value * 1;

    var temp = myArr[viTri1];
    myArr[viTri1] = myArr[viTri2];
    myArr[viTri2] = temp;
    getMyEle("showKetQuaMangChinh").innerHTML += `<p>Array sau khi swap vị trí : [${myArr}] </p>`
}


function tinhKetQua() {
    var numberValue = getMyEle("number").value.trim() * 1;
    myArr.push(numberValue);

    //Mảng số dương
    var soDuongArr = [];
    myArr.forEach(function (item) {
        if (item > 0) {
            soDuongArr.push(item);
        }
    });

    // Tổng các số dương
    var tongSoDuong = 0;
    soDuongArr.forEach(function (item) {
        tongSoDuong += item;
    });

    // Số dương nhỏ nhất    
    var soDuongNhoNhat = soDuongArr[0];
    for (i = 1; i < soDuongArr.length; i++) {
        currentNumber = soDuongArr[i];
        if (soDuongNhoNhat > currentNumber) {
            soDuongNhoNhat = currentNumber;
        }
    }

    // Số nhỏ nhất trong mảng    
    var soNhoNhat = myArr[0];
    for (i = 1; i < myArr.length; i++) {
        var currentNumber = myArr[i];
        if (soNhoNhat > currentNumber) {
            soNhoNhat = currentNumber;
        }
    }

    // Mảng số chẵn    
    var soChanArr = [];
    myArr.forEach(function (item) {
        if (item % 2 == 0) {
            soChanArr.push(item);
        } else {
            soChanArr.push(-1);
        }
    });

    // Swap vị trí 2 giá trị
    if (myArr.length == 2) { // Khi trong mảng có 2 phần tử mới xuất hiện input để swap
        getMyEle("divSwapGiaTri").style.display = "block";
    }

    // Sắp xếp mảng theo thứ tự tăng dần
    var soTangDanArr = [];
    myArr.forEach(function (item) {
        soTangDanArr.push(item);
    });

    for (i = 0; i < soTangDanArr.length - 1; i++) {
        for (j = i + 1; j < soTangDanArr.length; j++) {
            if (soTangDanArr[i] > soTangDanArr[j]) {
                var temp = soTangDanArr[i];
                soTangDanArr[i] = soTangDanArr[j];
                soTangDanArr[j] = temp;
            }
        }
    }

    // Tìm số nguyên tố đầu tiên
    var soNguyenToDauTien;
    for (i = 0; i < myArr.length; i++) {
        var flag = 0;
        // Nếu flag = 0 thì là số nguyên tố, ngược lại flag = 1 thì ko phải số nguyên tố
        if (myArr[i] < 2) {
            flag = 1; // Số bé hơn 2 luôn luôn ko là số nguyên tố
        }
        for (j = 2; j < myArr[i]; j++) {
            if (myArr[i] % j == 0) {  // Số nguyên tố là số chia hết cho 1 và chính nó, do đó nó tồn tại 2 ước số. 
                flag = 1;             // Đã loại 2 trường hợp này trong phần khởi tạo và điều kiện lặp nên tìm thấy bất kì ước số nào thì ko phải là số nguyên tố
            }
        }
        if (flag == 0) {
            soNguyenToDauTien = myArr[i];
            break;
        } else {
            soNguyenToDauTien = -1;
        }
    }

    // So sánh số lượng số âm và số dương
    var soAmArr = [];
    myArr.forEach(function (item) {
        if (item < 0) {
            soAmArr.push(item);
        }
    })

    var contentSoSanhSoLuongSoAmVaDuong;
    if (soDuongArr.length == soAmArr.length) {
        contentSoSanhSoLuongSoAmVaDuong = "Số lượng số dương = Số lượng số âm";
    } else if (soDuongArr.length > soAmArr.length) {
        contentSoSanhSoLuongSoAmVaDuong = "Số lượng số dương > Số lượng số âm";
    } else {
        contentSoSanhSoLuongSoAmVaDuong = "Số lượng số dương < Số lượng số âm";
    }

    getMyEle("showKetQuaMangChinh").style.display = "block";
    getMyEle("showKetQuaMangChinh").innerHTML = /*html */ `
   <p>My Array : [${myArr}]</p>
   <p>Tổng các số dương : ${tongSoDuong}</p>
   <p>Có ${soDuongArr.length} số dương</p>
   <p>Số nhỏ nhất : ${soNhoNhat}</p>
   <p>Số dương nhỏ nhất : ${soDuongNhoNhat}</p>
   <p>Số chẵn cuối cùng : ${soChanArr[soChanArr.length - 1]} </p>
   <p>Array sau khi sắp xếp tăng dần : [${soTangDanArr}]</p>
   <p>Số nguyên tố đầu tiên : ${soNguyenToDauTien}</p>
   <p>${contentSoSanhSoLuongSoAmVaDuong}</p>
   `
}

// Đếm số nguyên trong mảng số thực
var soThucArr = [];

function demSoNguyen() {
    var soThuc = getMyEle("soThuc").value.trim() * 1;
    soThucArr.push(soThuc);

    var soNguyenArr = [];

    for(i = 0; i < soThucArr.length; i++) {
        if(Math.ceil(soThucArr[i]) == Math.floor(soThucArr[i])) {
            soNguyenArr.push(soThucArr[i]);
        }
    }

   getMyEle("showKetQuaMangSoThuc").style.display = "block";
   getMyEle("showKetQuaMangSoThuc").innerHTML = `Có ${soNguyenArr.length} số nguyên`

}








